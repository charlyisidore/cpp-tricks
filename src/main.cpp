/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include "gzfstream.hpp"
#include "dopt.hpp"
#include "svg.hpp"

int main( int argc, char * argv[] )
{
	using dopt::arg;
	using dopt::var;

	// Default program options

	std::string input_file, output_file;
	std::set<std::string> extra_args;

	bool debug   = false,
	     verbose = true,
	     help    = false;

	int n = 0;

	// Option description

	dopt::command_line_parser parser;
	dopt::group options;

	options.name( "General options:" )
		( "debug",      "Enable debugging features",          var<true> ( &debug ) )
		( "quiet", 'q', "Do not display verbose information", var<false>( &verbose ) )
		( dopt::option::help(),                               var<true> ( &help ) )
	(
		dopt::group( "File options:" )
		( "input-file",  'i', "Input file",       arg( "FILE", &input_file ) )
		( "output-file", 'o', "Output file",      arg( "FILE" ) )
		(                'n', "An integer value", arg( "N" ).var( &n ) )
	);

	// Late binding

	options.bind( 'o', &output_file );
	options.bind( &extra_args );

	// Parse options

	parser.add( options );

	if ( !parser.run( argc, argv ) || help )
	{
		std::cout
			<< "Usage: " << argv[0] << " [OPTIONS]" << std::endl
			<< std::endl
			<< options << std::endl;
		return 0;
	}

	// Output parsing result

	if ( verbose )
	{
		std::clog
			<< "Program options:" << std::endl
			<< "\tinput-file  = " << input_file << std::endl
			<< "\toutput-file = " << output_file << std::endl
			<< "\tn           = " << n << std::endl
			<< "\tdebug       = " << debug << std::endl
			<< "\thelp        = " << help << std::endl
			<< "\t#extra-args = " << extra_args.size() << std::endl;
	}

	// Gzip input test

	if ( !input_file.empty() )
	{
		gz::ifstream gzip( input_file.c_str() );
		if ( gzip.is_open() )
		{
			std::string s;
			while ( std::getline( gzip, s ) )
			{
				std::cout << s << std::endl;
			}
		}
		else
		{
			std::cerr << "Error while opening '" << input_file << "'" << std::endl;
		}
	}

	// SVG test

	svg::document svgdoc( 600, 600 );
	svgdoc.view_box( 0, 0, 6, 6 );
	svgdoc
		<< svg::circle( 3, 3, n )
			.style( "fill:green;stroke:black" );

	// Gzip output test

	if ( !output_file.empty() )
	{
		gz::ofstream gzip( output_file.c_str() );
		if ( gzip.is_open() )
		{
			svgdoc.write( gzip );
		}
		else
		{
			std::cerr << "Error while opening '" << output_file << "'" << std::endl;
		}
	}

	return 0;
}

