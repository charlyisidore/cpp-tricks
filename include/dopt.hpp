/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DOPT_HPP
#define DOPT_HPP

#include <iostream>
#include <sstream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
#include <utility>
#include <stdexcept>
#include <iomanip>
#include <streambuf>
#include <cctype>
#include <getopt.h>

namespace dopt
{

/*
	Class: option_index

	Index class with multiple keys and multiple long names.
*/
class option_index
{
public:
	typedef        char   char_type;
	typedef std::string   string_type;

	// char comparator
	struct less_char
	{
		bool operator () ( char_type c1, char_type c2 ) const;
	};

	// string comparator
	struct less_string
	{
		bool operator () ( const string_type & s1, const string_type & s2 ) const;
	};

	typedef     std::set<char_type, less_char>   set_char_type;
	typedef std::set<string_type, less_string>   set_string_type;

	/*
		Constructor: option_index
	*/
	option_index();

	/*
		Constructor: option_index

		Parameters:

			c - A key for this index.
	*/
	option_index( char_type c );

	/*
		Constructor: option_index

		Parameters:

			name - A long name for this index.
	*/
	option_index( const string_type & name );

	/*
		Constructor: option_index

		Parameters:

			c - A key for this index.
			name - A long name for this index.
	*/
	option_index( char_type c, const string_type & name );

	/*
		Method: insert

		Assign a key for this index.
	*/
	option_index & insert( char_type c );

	/*
		Method: insert

		Assign a long name for this index.
	*/
	option_index & insert( const string_type & name );

	/*
		Method: has

		Check whether a key is assigned for this index.
	*/
	bool has( char_type c ) const;

	/*
		Method: has

		Check whether a long name is assigned for this index.
	*/
	bool has( const string_type & name ) const;

	/*
		Method: keys

		Return the set of keys assigned for this index.
	*/
	const set_char_type & keys() const;

	/*
		Method: names

		Return the set of long names assigned for this index.
	*/
	const set_string_type & names() const;

	/*
		Method: front

		Return the first key or character assigned for this index.
	*/
	char_type front() const;

	/*
		Method: front_key

		Return the first key assigned for this index.
	*/
	char_type front_key() const;

	/*
		Method: front_name

		Return the first long name assigned for this index.
	*/
	string_type front_name() const;

	/*
		Method: compare

		Compare this index with another index.
	*/
	bool compare( const option_index & x ) const;

	/*
		Method: empty

		Return true if the index is empty, false otherwise.
	*/
	bool empty() const;

private:
	set_char_type _key;
	set_string_type _name;
};

/*
	Function: operator <

	Compare two option indices.
*/
bool operator < ( const option_index & x1, const option_index & x2 );

////////////////////////////////////////////////////////////////////////////////

/*
	Class: argument

	This class specifies an option argument.
*/
class argument
{
public:
	typedef std::string string_type;
	typedef bool (*callback_type)( char c, const char * s, const char * arg, void * user );
	typedef std::pair<callback_type, void *> callback_pair_type;

	/*
		Constructor: argument

		Parameters:

			name - Name for this argument.
			cb - A callback associated with this option.
			user - A callback associated with this option.
	*/
	argument( const string_type & name = "", callback_type cb = 0, void * user = 0 );

	/*
		Method: name

		Set the name for this argument.
	*/
	argument & name( const string_type & name );

	/*
		Method: optional

		Make this argument optional.
	*/
	argument & optional();

	/*
		Method: required

		Make this argument required.
	*/
	argument & required();

	/*
		Method: var<T>

		Build a callback to assign a variable.
	*/
	template<typename T>
	argument & var( T * x );

	/*
		Method: var<bool>

		Build a callback to assign a boolean variable.
	*/
	template<bool V>
	argument & var( bool * x );

	/*
		Method: var<char>

		Build a callback to assign a character variable.
	*/
	template<char V>
	argument & var( char * x );

	/*
		Method: var<int>

		Build a callback to assign a integer variable.
	*/
	template<int V>
	argument & var( int * x );

	/*
		Method: callback

		Set a callback function for this argument.
	*/
	argument & callback( callback_type cb );

	/*
		Method: callback

		Set a callback function for this argument.
	*/
	argument & callback( callback_type cb, void * user );

	/*
		Method: callback

		Set a callback function for this argument.
	*/
	argument & callback( callback_pair_type cb );

	/*
		Method: userdata

		Set a user-defined data for this argument.
	*/
	argument & userdata( void * user );

	/*
		Method: empty

		Return true if the argument does not exist, false otherwise.
	*/
	bool empty() const;

	/*
		Method: name

		Return the name for this argument.
	*/
	const string_type & name() const;

	/*
		Method: is_optional

		Return true if the argument is optional, false otherwise.
	*/
	bool is_optional() const;

	/*
		Method: is_required

		Return true if the argument is required, false otherwise.
	*/
	bool is_required() const;

	/*
		Method: callback

		Return a callback function for this argument.
	*/
	callback_type callback() const;

	/*
		Method: userdata

		Return a user-defined data for this argument.
	*/
	void * userdata() const;

	/*
		Method: null

		Return an empty argument.
	*/
	static argument null();

	/*
		Method: var_callback<T>

		Callback that assigns a variable depending on the option argument.
	*/
	template<typename T>
	static bool var_callback( char c, const char * name, const char * arg, void * user );

	/*
		Method: var_callback<T,V>

		Callback that assigns a variable using a predefined argument.
	*/
	template<typename T, T V>
	static bool var_callback( char c, const char * name, const char * arg, void * user );

private:
	string_type _name;
	bool _optional;
	callback_type _callback;
	void * _userdata;
};

// Helper functions

/*
	Function: arg

	Create an argument.

	Parameters:

		name - Argument name.
*/
argument arg( const argument::string_type & name );

/*
	Function: arg<T>

	Create an argument that assigns a variable.

	Parameters:

		name - Argument name.
		x - A pointer to a variable.
*/
template<typename T>
argument arg( const argument::string_type & name, T * x );

/*
	Function: var<T>

	Create a callback to assign a variable.

	Parameters:

		x - A pointer to a variable.
*/
template<typename T>
argument::callback_pair_type var( T * x );

/*
	Function: var<bool>

	Create a callback to assign a boolean variable with a predefined value.

	Parameters:

		x - A pointer to a variable.
*/
template<bool V>
argument::callback_pair_type var( bool * x );

/*
	Function: var<char>

	Create a callback to assign a character variable with a predefined value.

	Parameters:

		x - A pointer to a variable.
*/
template<char V>
argument::callback_pair_type var( char * x );

/*
	Function: var<int>

	Create a callback to assign an integer variable with a predefined value.

	Parameters:

		x - A pointer to a variable.
*/
template<int V>
argument::callback_pair_type var( int * x );

////////////////////////////////////////////////////////////////////////////////

/*
	Class: option

	This class specifies a single option.
*/
class option
{
public:
	typedef                  option_index   index_type;
	typedef                      argument   argument_type;
	typedef       option_index::char_type   char_type;
	typedef     option_index::string_type   string_type;
	typedef   option_index::set_char_type   set_char_type;
	typedef option_index::set_string_type   set_string_type;
	typedef  argument_type::callback_type   callback_type;

	/*
		Constructor: option
	*/
	option();

	/*
		Constructor: option

		Parameters:

			c - A key for this index.
			doc - Documentation string for this option.
			arg - Argument associated with this option.
	*/
	option( char_type c, const string_type & doc = "", const argument_type & arg = argument_type::null() );

	/*
		Constructor: option

		Parameters:

			name - A long name for this index.
			doc - Documentation string for this option.
			arg - Argument associated with this option.
	*/
	option( const string_type & name, const string_type & doc = "", const argument_type & arg = argument_type::null() );

	/*
		Constructor: option

		Parameters:

			name - A long name for this index.
			c - A key for this index.
			doc - Documentation string for this option.
			arg - Argument associated with this option.
	*/
	option( const string_type & name, char_type c, const string_type & doc = "", const argument_type & arg = argument_type::null() );

	/*
		Method: index

		Return the index for this option.
	*/
	const index_type & index() const;

	/*
		Method: doc

		Return the documentation string for this option.
	*/
	const string_type & doc() const;

	/*
		Method: arg

		Return the argument associated with this option.
	*/
	const argument_type & arg() const;

	/*
		Method: arg

		Return the argument associated with this option.
	*/
	argument_type & arg();

	/*
		Method: index

		Return the index for this option.
	*/
	index_type & index();

	/*
		Method: doc

		Set the documentation string for this option.
	*/
	option & doc( const string_type & doc );

	/*
		Method: arg

		Set the argument associated with this option.
	*/
	option & arg( const argument_type & arg );

	/*
		Method: compare

		Compare this option with another option.
	*/
	bool compare( const option & x ) const;

	/*
		Method: empty

		Return true if this option is empty, false otherwise.
	*/
	bool empty() const;

	/*
		Method: write

		Output this option for a --help message.

		Parameters:

			os - Output stream.
			key_col - Starting column for keys. The default is 2.
			name_col - Starting column for long names. The default is 6.
			doc_col - Starting column for documentation. The default is 29.
			rmargin - Last column. The default is 79.
	*/
	void write( std::ostream & os, int key_col = 2, int name_col = 6, int doc_col = 29, int rmargin = 79 ) const;

	/*
		Method: operator <<

		Alias of insert.
	*/
	option & operator << ( char_type c );

	/*
		Method: operator <<

		Alias of insert.
	*/
	option & operator << ( const string_type & name );

	/*
		Method: help

		Return a built-in -?, --help option.
	*/
	static option help();

	/*
		Method: usage

		Return a built-in --usage option.
	*/
	static option usage();

	/*
		Method: version

		Return a built-in -V, --version option.
	*/
	static option version();

	/*
		Method: null

		Return an empty option.
	*/
	static option null();

private:
	index_type _index;
	string_type _doc;
	argument_type _arg;
};

/*
	Function: operator <

	Compare two options.
*/
bool operator < ( const option & opt1, const option & opt2 );

/*
	Function: operator <<

	Output an option for a --help message.
*/
std::ostream & operator << ( std::ostream & os, const option & opt );

// Functor to search for options by key
struct option_has_key
{
	typedef option::char_type char_type;
	option_has_key( char_type k ) : key( k ) {}
	bool operator () ( const option & opt ) const { return opt.index().has( key ); }
	char_type key;
};

// Functor to search for options by long name
struct option_has_name
{
	typedef option::string_type string_type;
	option_has_name( const string_type & n ) : name( n ) {}
	bool operator () ( const option & opt ) const { return opt.index().has( name ); }
	string_type name;
};

////////////////////////////////////////////////////////////////////////////////

/*
	Class: group

	This class specifies a group of options.
*/
class group
{
public:
	typedef                            option   option_type;
	typedef        option_type::argument_type   argument_type;
	typedef            option_type::char_type   char_type;
	typedef          option_type::string_type   string_type;
	typedef argument_type::callback_pair_type   callback_pair_type;

	/*
		Constructor: group

		Parameters:

			name - Title of the option group.
	*/
	group( const string_type & name = "" );

	/*
		Method: name

		Return the title of the option group.
	*/
	const string_type & name() const;

	/*
		Method: name

		Set the title of the option group.
	*/
	group & name( const string_type & name );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			opt - A single option.
	*/
	group & operator () ( const option_type & opt );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			opt - A single option.
			cb - A callback object.
	*/
	group & operator () ( const option_type & opt, const callback_pair_type & cb );

	/*
		Method: operator ()

		Append another option group to this option group.

		Parameters:

			grp - An option group.
	*/
	group & operator () ( const group & grp );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			c - A key assigned for the new option.
			doc - Documentation string for the new option.
			arg - An argument associated with the new option.
	*/
	group & operator () ( char_type c, const string_type & doc = "", const argument_type & arg = argument_type::null() );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			c - A key assigned for the new option.
			doc - Documentation string for the new option.
			cb - A callback object.
	*/
	group & operator () ( char_type c, const string_type & doc, const callback_pair_type & cb );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			name - A long name for the new option.
			doc - Documentation string for the new option.
			arg - An argument associated with the new option.
	*/
	group & operator () ( const string_type & name, const string_type & doc = "", const argument_type & arg = argument_type::null() );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			name - A long name for the new option.
			doc - Documentation string for the new option.
			cb - A callback object.
	*/
	group & operator () ( const string_type & name, const string_type & doc, const callback_pair_type & cb );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			name - A long name for the new option.
			c - A key for the new option.
			doc - Documentation string for the new option.
			arg - An argument associated with the new option.
	*/
	group & operator () ( const string_type & name, char_type c, const string_type & doc = "", const argument_type & arg = argument_type::null() );

	/*
		Method: operator ()

		Append a single option to this option group.

		Parameters:

			name - A long name for the new option.
			c - A key for the new option.
			doc - Documentation string for the new option.
			cb - A callback object.
	*/
	group & operator () ( const string_type & name, char_type c, const string_type & doc, const callback_pair_type & cb );

	/*
		Method: bind

		Bind a callback to an option.

		Parameters:

			c - A key of the option.
			cb - A callback to be associated with the option.
	*/
	group & bind( char_type c, const callback_pair_type & cb );

	/*
		Method: bind

		Bind a callback to an option.

		Parameters:

			name - A long name of the option.
			cb - A callback to be associated with the option.
	*/
	group & bind( const string_type & name, const callback_pair_type & cb );

	/*
		Method: bind

		Bind a callback to program arguments (not associated to options).

		Parameters:

			cb - An callback to be associated with the group.
	*/
	group & bind( const callback_pair_type & cb );

	/*
		Method: bind<T>

		Bind a variable for assignment.

		Parameters:

			x - A pointer to a variable.
	*/
	template<typename T>
	group & bind( char_type c, T * x );
	template<typename T>
	group & bind( const string_type & name, T * x );

	/*
		Method: bind<bool>

		Bind a boolean variable for assignment.

		Parameters:

			x - A pointer to a variable.
	*/
	template<bool V>
	group & bind( char_type c, bool * x );
	template<bool V>
	group & bind( const string_type & name, bool * x );

	/*
		Method: bind<char>

		Bind a character variable for assignment.

		Parameters:

			x - A pointer to a variable.
	*/
	template<char V>
	group & bind( char_type c, char * x );
	template<char V>
	group & bind( const string_type & name, char * x );

	/*
		Method: bind<int>

		Bind an integer variable for assignment.

		Parameters:

			x - A pointer to a variable.
	*/
	template<int V>
	group & bind( char_type c, int * x );
	template<int V>
	group & bind( const string_type & name, int * x );

	/*
		Method: bind

		Bind a variable to program arguments (not associated to options).

		Parameters:

			x - A pointer to a variable.
	*/
	template<typename T>
	group & bind( T * x );

	/*
		Method: write

		Output a --help message.

		Parameters:

			os - Output stream.
			header_col - Starting column for header. The default is 1.
			key_col - Starting column for keys. The default is 2.
			name_col - Starting column for long names. The default is 6.
			doc_col - Starting column for documentation. The default is 29.
			rmargin - Last column. The default is 79.
	*/
	void write( std::ostream & os, int header_col = 1, int key_col = 2, int name_col = 6, int doc_col = 29, int rmargin = 79 ) const;

	// Return the group argument.
	const argument_type & arg() const;

	// Return the list of options.
	const std::vector<option_type> & options() const;

	// Return the list of options.
	const std::vector<group> & children() const;

private:
	// Search for an option
	option_type * find( char_type c );
	option_type * find( const string_type & name );

	string_type _name;
	std::vector<option_type> _options;
	argument_type _arg;
	std::vector<group> _children;
};

/*
	Function: operator <<

	Output a --help message.
*/
std::ostream & operator << ( std::ostream & os, const group & grp );

////////////////////////////////////////////////////////////////////////////////

/*
	Class: command_line_parser

	A command line parser.
*/
class command_line_parser
{
public:
	typedef                     option   option_type;
	typedef                      group   group_type;
	typedef option_type::callback_type   callback_type;

	/*
		Constructor: command_line_parser
	*/
	command_line_parser();

	/*
		Constructor: command_line_parser

		Parameters:

			cb - A parser callback function.
			user - Custom data passed as an argument of the callback function.
	*/
	command_line_parser( callback_type cb, void * user = 0 );

	/*
		Constructor: command_line_parser

		Parameters:

			grp - An option group.
	*/
	command_line_parser( const group_type & grp );

	/*
		Constructor: command_line_parser

		Parameters:

			grp - An option group.
			cb - A parser callback function.
			user - Custom data passed as an argument of the callback function.
	*/
	command_line_parser( const group_type & grp, callback_type cb, void * user = 0 );

	/*
		Method: add

		Append an option to the parser.

		Parameters:

			opt - A single option.
	*/
	command_line_parser & add( const option_type & opt );

	/*
		Method: add

		Append an option group to the parser.

		Parameters:

			grp - An option group.
	*/
	command_line_parser & add( const group_type & grp );

	/*
		Method: callback

		Set the parser callback function.

		Parameters:

			cb - A parser callback function.
			user - Custom data passed as an argument of the callback function.
	*/
	command_line_parser & callback( callback_type cb, void * user = 0 );

	/*
		Method: callback

		Return the parser callback function.
	*/
	callback_type callback() const;

	/*
		Method: userdata

		Return the custom data passed as an argument of the callback function.
	*/
	void * userdata() const;

	/*
		Method: clear

		Removes all the options from the parser.
	*/
	void clear();

	/*
		Method: run

		Parse the main() arguments.

		Parameters:

			argc - Number of main() arguments.
			argv - Pointer to the main() arguments.

		Returns:

			true if successful, false otherwise.
	*/
	bool run( int argc, char * argv[] ) const;

	/*
		Method: run

		Parse the main() arguments and force a custom callback function.

		Parameters:

			argc - Number of main() arguments.
			argv - Pointer to the main() arguments.
			cb - A callback function.
			user - Custom data passed as an argument of the callback function.

		Returns:

			true if successful, false otherwise.
	*/
	bool run( int argc, char * argv[], callback_type cb, void * user = 0 ) const;

	// Built-in default parser.
	bool default_parser( char c, const char * name, const char * arg ) const;

	// Built-in static default parser.
	static bool default_parser( char c, const char * name, const char * arg, void * user );

	// Built-in static debug parser.
	static bool debug_parser( char c, const char * name, const char * arg, void * user );

private:
	std::vector<option_type> _options;
	callback_type _callback;
	void * _userdata;
	callback_type _callback_arg;
	void * _userdata_arg;
};

////////////////////////////////////////////////////////////////////////////////

/*
	Class: error

	Base class for exceptions.
*/
class error : public std::exception
{
};

/*
	Class: invalid_value_error

	Exception for invalid option values.
*/
class invalid_value_error : public error, public std::runtime_error
{
public:
	invalid_value_error( char key, const char * name );
	virtual ~invalid_value_error() throw() {}
	virtual const char * what() const throw();

private:
	char _key;
	const char * _name;
	std::string _what;
};

////////////////////////////////////////////////////////////////////////////////

// A word wrap buffer.
class wrapbuf : public std::streambuf
{
public:
	typedef std::size_t size_type;
	typedef std::basic_string<char_type> string_type;

	wrapbuf( std::streambuf * s, size_type w );
	wrapbuf( std::streambuf * s, size_type indentw, size_type w );
	~wrapbuf();

	void indent( size_type indentw );
	size_type indent() const;

	void width( size_type w );
	size_type width() const;

	void first( int w );
	int first() const;

private:
	virtual int_type overflow( int_type c = traits_type::eof() );
	void _put_margin();

	static const int _tab_width = 8;
	std::streambuf * _sbuf;
	size_type _indent,
	          _width,
	          _count;
	string_type _buffer;
	int _first;
};

// A word wrap stream.
class wrapstream : public std::ostream
{
public:
	typedef std::size_t size_type;

	wrapstream( std::ostream & os, size_type w = 80 );
	wrapstream( std::ostream & os, size_type indentw, size_type w );

	wrapstream & indent( size_type indentw );
	size_type indent() const;

	wrapstream & wrap( size_type w );
	wrapstream & wrap( size_type indentw, size_type w );

	wrapstream & first( size_type w );

private:
	wrapbuf _buf;
};

////////////////////////////////////////////////////////////////////////////////

inline bool option_index::less_char::operator () ( char_type c1, char_type c2 ) const
{
	if ( std::isalnum( c1 ) && std::isalnum( c2 ) )
	{
		return std::tolower( c1 ) < std::tolower( c2 )
		  || ( std::tolower( c1 ) == std::tolower( c2 ) && c2 < c1 );
	}
	else
	{
		return std::isalnum( c1 ) || ( !std::isalnum( c2 ) && c1 < c2 );
	}
}

inline bool option_index::less_string::operator () ( const string_type & s1, const string_type & s2 ) const
{
	return std::lexicographical_compare( s1.begin(), s1.end(), s2.begin(), s2.end(), less_char() );
}

inline option_index::option_index()
{
}

inline option_index::option_index( char_type c )
{
	insert( c );
}

inline option_index::option_index( const string_type & name )
{
	insert( name );
}

inline option_index::option_index( char_type c, const string_type & name )
{
	insert( c );
	insert( name );
}

inline option_index & option_index::insert( char_type c )
{
	_key.insert( c );
	return *this;
}

inline option_index & option_index::insert( const string_type & name )
{
	_name.insert( name );
	return *this;
}

inline bool option_index::has( char_type c ) const
{
	return _key.find( c ) != _key.end();
}

inline bool option_index::has( const string_type & name ) const
{
	return _name.find( name ) != _name.end();
}

inline const option_index::set_char_type & option_index::keys() const
{
	return _key;
}

inline const option_index::set_string_type & option_index::names() const
{
	return _name;
}

inline option_index::char_type option_index::front() const
{
	return !_key.empty() ? *_key.begin() : ( !_name.empty() ? (*_name.begin())[0] : 0 );
}

inline option_index::char_type option_index::front_key() const
{
	return !_key.empty() ? *_key.begin() : 0;
}

inline option_index::string_type option_index::front_name() const
{
	return !_name.empty() ? *_name.begin() : string_type();
}

inline bool option_index::compare( const option_index & x ) const
{
	char_type c1 = front(), c2 = x.front();

	if ( c1 == c2 )
	{
		bool h1 = has( c1 ), h2 = x.has( c1 );

		if ( h1 && h2 )
		{
			return false;
		}
		else if ( h1 || h2 )
		{
			return h1;
		}
		else
		{
			return less_string()( front_name(), x.front_name() );
		}
	}
	return less_char()( c1, c2 );
}

inline bool option_index::empty() const
{
	return _key.empty() && _name.empty();
}

inline bool operator < ( const option_index & x1, const option_index & x2 )
{
	return x1.compare( x2 );
}

////////////////////////////////////////////////////////////////////////////////

inline argument::argument( const string_type & name, callback_type cb, void * user ) :
	_name( name ),
	_optional( false ),
	_callback( cb ),
	_userdata( user )
{
}

inline argument & argument::name( const string_type & name )
{
	_name = name;
	return *this;
}

inline argument & argument::optional()
{
	_optional = true;
	return *this;
}

inline argument & argument::required()
{
	_optional = false;
	return *this;
}

template<typename T>
inline argument & argument::var( T * x )
{
	_callback = var_callback<T>;
	_userdata = static_cast<void *>( x );
	return *this;
}

template<bool V>
inline argument & argument::var( bool * x )
{
	_callback = var_callback<bool,V>;
	_userdata = static_cast<void *>( x );
	return *this;
}

template<char V>
inline argument & argument::var( char * x )
{
	_callback = var_callback<char,V>;
	_userdata = static_cast<void *>( x );
	return *this;
}

template<int V>
inline argument & argument::var( int * x )
{
	_callback = var_callback<int,V>;
	_userdata = static_cast<void *>( x );
	return *this;
}

inline argument & argument::callback( callback_type cb )
{
	_callback = cb;
	return *this;
}

inline argument & argument::callback( callback_type cb, void * user )
{
	_callback = cb;
	_userdata = user;
	return *this;
}

inline argument & argument::callback( callback_pair_type cb )
{
	_callback = cb.first;
	_userdata = cb.second;
	return *this;
}

inline argument & argument::userdata( void * user )
{
	_userdata = user;
	return *this;
}

inline bool argument::empty() const
{
	return _name.empty();
}

inline const argument::string_type & argument::name() const
{
	return _name;
}

inline bool argument::is_optional() const
{
	return _optional;
}

inline bool argument::is_required() const
{
	return !_optional;
}

inline argument::callback_type argument::callback() const
{
	return _callback;
}

inline void * argument::userdata() const
{
	return _userdata;
}

inline argument argument::null()
{
	return argument();
}

template<typename T>
inline bool argument::var_callback( char c, const char * name, const char * arg, void * user )
{
	if ( user )
	{
		if ( arg )
		{
			std::istringstream is( arg );
			is >> *static_cast<T *>( user );
			if ( is.fail() )
			{
				throw invalid_value_error( c, name );
			}
		}
		return true;
	}
	return false;
}

template<>
inline bool argument::var_callback<std::string>( char c, const char * name, const char * arg, void * user )
{
	if ( user )
	{
		if ( arg )
		{
			*static_cast<std::string *>( user ) = arg;
		}
		return true;
	}
	return false;
}

template<>
inline bool argument::var_callback< std::vector<std::string> >( char c, const char * name, const char * arg, void * user )
{
	if ( user )
	{
		if ( arg )
		{
			static_cast<std::vector<std::string> *>( user )->push_back( arg );
		}
		return true;
	}
	return false;
}

template<>
inline bool argument::var_callback< std::set<std::string> >( char c, const char * name, const char * arg, void * user )
{
	if ( user )
	{
		if ( arg )
		{
			static_cast<std::set<std::string> *>( user )->insert( arg );
		}
		return true;
	}
	return false;
}

template<typename T, T V>
inline bool argument::var_callback( char c, const char * name, const char * arg, void * user )
{
	if ( user )
	{
		if ( arg )
		{
			var_callback<T>( c, name, arg, user );
		}
		else
		{
			*static_cast<T *>( user ) = V;
		}
		return true;
	}
	return false;
}

inline argument arg( const argument::string_type & name )
{
	return argument( name );
}

template<typename T>
inline argument arg( const argument::string_type & name, T * x )
{
	return argument( name, argument::var_callback<T>, static_cast<void *>( x ) );
}

template<typename T>
inline argument::callback_pair_type var( T * x )
{
	return argument::callback_pair_type( argument::var_callback<T>, static_cast<void *>( x ) );
}

template<bool V>
inline argument::callback_pair_type var( bool * x )
{
	return argument::callback_pair_type( argument::var_callback<bool,V>, static_cast<void *>( x ) );
}

template<char V>
inline argument::callback_pair_type var( char * x )
{
	return argument::callback_pair_type( argument::var_callback<char,V>, static_cast<void *>( x ) );
}

template<int V>
inline argument::callback_pair_type var( int * x )
{
	return argument::callback_pair_type( argument::var_callback<int,V>, static_cast<void *>( x ) );
}

////////////////////////////////////////////////////////////////////////////////

inline option::option()
{
}

inline option::option( char_type c, const string_type & doc, const argument_type & arg ) :
	_doc( doc ),
	_arg( arg )
{
	_index.insert( c );
}

inline option::option( const string_type & name, const string_type & doc, const argument_type & arg ) :
	_doc( doc ),
	_arg( arg )
{
	_index.insert( name );
}

inline option::option( const string_type & name, char_type c, const string_type & doc, const argument_type & arg ) :
	_doc( doc ),
	_arg( arg )
{
	_index.insert( c ).insert( name );
}

inline const option::index_type & option::index() const
{
	return _index;
}

inline const option::string_type & option::doc() const
{
	return _doc;
}

inline const option::argument_type & option::arg() const
{
	return _arg;
}

inline option::argument_type & option::arg()
{
	return _arg;
}

inline option::index_type & option::index()
{
	return _index;
}

inline option & option::doc( const string_type & doc )
{
	_doc = doc;
	return *this;
}

inline option & option::arg( const argument_type & arg )
{
	_arg = arg;
	return *this;
}

inline bool option::compare( const option & x ) const
{
	return _index.compare( x.index() );
}

inline bool option::empty() const
{
	return _index.empty();
}

inline void option::write( std::ostream & os, int key_col, int name_col, int doc_col, int rmargin ) const
{
	wrapstream docwrap( os, doc_col, rmargin - doc_col );
	set_char_type::const_iterator k;
	set_string_type::const_iterator n;
	std::ostringstream buf;

	buf << string_type( index().keys().empty() ? name_col : key_col, ' ' );

	for ( k = index().keys().begin(); k != index().keys().end(); ++k )
	{
		if ( k != index().keys().begin() )
		{
			buf << ", ";
		}
		buf << "-" << char( *k );

		// Argument
		if ( !arg().empty() && index().names().empty() )
		{
			if ( arg().is_optional() )
			{
				buf << "[" << arg().name() << "]";
			}
			else
			{
				buf << " " << arg().name();
			}
		}
	}

	if ( !index().keys().empty() && !index().names().empty() )
	{
		buf << ", ";
	}

	for ( n = index().names().begin(); n != index().names().end(); ++n )
	{
		if ( n != index().names().begin() )
		{
			buf << ", ";
		}
		buf << "--" << *n;

		// Argument
		if ( !arg().empty() )
		{
			if ( arg().is_optional() )
			{
				buf << "[=" << arg().name() << "]";
			}
			else
			{
				buf << "=" << arg().name();
			}
		}
	}

	os << std::left << std::setw( doc_col ) << buf.str();

	if ( int( buf.str().length() ) > doc_col-2 )
	{
		os << std::endl
		   << string_type( doc_col, ' ' );
	}

	docwrap.first( 0 ) << doc();
}

inline option & option::operator << ( char_type c )
{
	index().insert( c );
	return *this;
}

inline option & option::operator << ( const string_type & name )
{
	index().insert( name );
	return *this;
}

inline bool operator < ( const option & opt1, const option & opt2 )
{
	return opt1.compare( opt2 );
}

inline option option::help()
{
	return option().doc( "Give this help list" ) << '?' << "help";
}

inline option option::usage()
{
	return option().doc( "Give a short usage message" ) << "usage";
}

inline option option::version()
{
	return option().doc( "Print program version" ) << 'V' << "version";
}

inline option option::null()
{
	return option();
}

inline std::ostream & operator << ( std::ostream & os, const option & opt )
{
	opt.write( os );
	return os;
}

////////////////////////////////////////////////////////////////////////////////

inline group::group( const string_type & name ) :
	_name( name )
{
}

inline const group::string_type & group::name() const
{
	return _name;
}

inline group & group::name( const string_type & name )
{
	_name = name;
	return *this;
}

inline group & group::operator () ( const option_type & opt )
{
	_options.push_back( opt );
	return *this;
}

inline group & group::operator () ( const option_type & opt, const callback_pair_type & cb )
{
	option_type copy( opt );
	copy.arg().callback( cb );
	_options.push_back( copy );
	return *this;
}

inline group & group::operator () ( const group & grp )
{
	group copy( grp );
	std::vector<group>::const_iterator j;

	// Do not allow children groups to have children.
	copy._children.clear();
	_children.push_back( copy );

	// Copy grand-children to the parent list instead.
	for ( j = grp._children.begin(); j != grp._children.end(); ++j )
	{
		_children.push_back( *j );
	}
	return *this;
}

inline group & group::operator () ( char_type c, const string_type & doc, const argument_type & arg )
{
	option_type opt;
	opt.index().insert( c );
	opt.doc( doc );
	opt.arg( arg );
	operator () ( opt );
	return *this;
}

inline group & group::operator () ( char_type c, const string_type & doc, const callback_pair_type & cb )
{
	option_type opt;
	opt.index().insert( c );
	opt.doc( doc );
	operator () ( opt, cb );
	return *this;
}

inline group & group::operator () ( const string_type & name, const string_type & doc, const argument_type & arg )
{
	option_type opt;
	opt.index().insert( name );
	opt.doc( doc );
	opt.arg( arg );
	operator () ( opt );
	return *this;
}

inline group & group::operator () ( const string_type & name, const string_type & doc, const callback_pair_type & cb )
{
	option_type opt;
	opt.index().insert( name );
	opt.doc( doc );
	operator () ( opt, cb );
	return *this;
}

inline group & group::operator () ( const string_type & name, char_type c, const string_type & doc, const argument_type & arg )
{
	option_type opt;
	opt.index().insert( c );
	opt.index().insert( name );
	opt.doc( doc );
	opt.arg( arg );
	operator () ( opt );
	return *this;
}

inline group & group::operator () ( const string_type & name, char_type c, const string_type & doc, const callback_pair_type & cb )
{
	option_type opt;
	opt.index().insert( c );
	opt.index().insert( name );
	opt.doc( doc );
	operator () ( opt, cb );
	return *this;
}

inline group & group::bind( char_type c, const callback_pair_type & cb )
{
	option_type * opt = find( c );

	if ( opt )
	{
		opt->arg().callback( cb );
	}
	return *this;
}

inline group & group::bind( const string_type & name, const callback_pair_type & cb )
{
	option_type * opt = find( name );

	if ( opt )
	{
		opt->arg().callback( cb );
	}
	return *this;
}

inline group & group::bind( const callback_pair_type & cb )
{
	_arg.callback( cb );
	return *this;
}

template<typename T>
inline group & group::bind( char_type c, T * x )
{
	bind( c, var<T>( x ) );
	return *this;
}

template<typename T>
inline group & group::bind( const string_type & name, T * x )
{
	bind( name, var<T>( x ) );
	return *this;
}

template<bool V>
inline group & group::bind( char_type c, bool * x )
{
	bind( c, var<V>( x ) );
	return *this;
}

template<bool V>
inline group & group::bind( const string_type & name, bool * x )
{
	bind( name, var<V>( x ) );
	return *this;
}

template<char V>
inline group & group::bind( char_type c, char * x )
{
	bind( c, var<V>( x ) );
	return *this;
}
template<char V>
inline group & group::bind( const string_type & name, char * x )
{
	bind( name, var<V>( x ) );
	return *this;
}

template<int V>
inline group & group::bind( char_type c, int * x )
{
	bind( c, var<V>( x ) );
	return *this;
}

template<int V>
inline group & group::bind( const string_type & name, int * x )
{
	bind( name, var<V>( x ) );
	return *this;
}

template<typename T>
inline group & group::bind( T * x )
{
	bind( var<T>( x ) );
	return *this;
}

inline void group::write( std::ostream & os, int header_col, int key_col, int name_col, int doc_col, int rmargin ) const
{
	std::vector<option_type>::const_iterator i;
	std::vector<group>::const_iterator j;

	if ( !name().empty() )
	{
		os << string_type( header_col, ' ' ) << name() << std::endl;
	}

	for ( i = _options.begin(); i != _options.end(); ++i )
	{
		i->write( os, key_col, name_col, doc_col, rmargin );
	}

	for ( j = _children.begin(); j != _children.end(); ++j )
	{
		if ( j != _children.begin() || !_options.empty() )
		{
			os << std::endl;
		}
		j->write( os, header_col, key_col, name_col, doc_col, rmargin );
	}
}

inline const group::argument_type & group::arg() const
{
	return _arg;
}

inline const std::vector<group::option_type> & group::options() const
{
	return _options;
}

inline const std::vector<group> & group::children() const
{
	return _children;
}

inline group::option_type * group::find( char_type c )
{
	std::vector<option_type>::iterator i;
	std::vector<group>::iterator j;

	i = std::find_if( _options.begin(), _options.end(), option_has_key( c ) );

	if ( i != _options.end() )
	{
		return &(*i);
	}

	for ( j = _children.begin(); j != _children.end(); ++j )
	{
		option_type * opt = j->find( c );
		if ( opt )
		{
			return opt;
		}
	}
	return 0;
}

inline group::option_type * group::find( const string_type & name )
{
	std::vector<option_type>::iterator i;
	std::vector<group>::iterator j;

	i = std::find_if( _options.begin(), _options.end(), option_has_name( name ) );

	if ( i != _options.end() )
	{
		return &(*i);
	}

	for ( j = _children.begin(); j != _children.end(); ++j )
	{
		option_type * opt = j->find( name );
		if ( opt )
		{
			return opt;
		}
	}
	return 0;
}

inline std::ostream & operator << ( std::ostream & os, const group & grp )
{
	grp.write( os );
	return os;
}

////////////////////////////////////////////////////////////////////////////////

inline command_line_parser::command_line_parser() :
	_callback( default_parser ),
	_userdata( this ),
	_callback_arg( 0 ),
	_userdata_arg( 0 )
{
}

inline command_line_parser::command_line_parser( callback_type cb, void * user ) :
	_callback( cb ),
	_userdata( user ),
	_callback_arg( 0 ),
	_userdata_arg( 0 )
{
}

inline command_line_parser::command_line_parser( const group_type & grp ) :
	_callback( default_parser ),
	_userdata( this ),
	_callback_arg( 0 ),
	_userdata_arg( 0 )
{
	add( grp );
}

inline command_line_parser::command_line_parser( const group_type & grp, callback_type cb, void * user ) :
	_callback( cb ),
	_userdata( user ),
	_callback_arg( 0 ),
	_userdata_arg( 0 )
{
	add( grp );
}

inline command_line_parser & command_line_parser::add( const group & grp )
{
	std::vector<option_type>::const_iterator i;
	std::vector<group_type>::const_iterator j;

	for ( i = grp.options().begin(); i != grp.options().end(); ++i )
	{
		add( *i );
	}

	for ( j = grp.children().begin(); j != grp.children().end(); ++j )
	{
		add( *j );
	}

	_callback_arg = grp.arg().callback();
	_userdata_arg = grp.arg().userdata();
	return *this;
}

inline command_line_parser & command_line_parser::add( const option_type & opt )
{
	_options.push_back( opt );
	return *this;
}

inline command_line_parser & command_line_parser::callback( callback_type cb, void * user )
{
	_callback = cb;
	_userdata = user;
	return *this;
}

inline command_line_parser::callback_type command_line_parser::callback() const
{
	return _callback;
}

inline void * command_line_parser::userdata() const
{
	return _userdata;
}

inline void command_line_parser::clear()
{
	_options.clear();
	_callback_arg = 0;
	_userdata_arg = 0;
}

inline bool command_line_parser::run( int argc, char * argv[] ) const
{
	return run( argc, argv, _callback, _userdata );
}

inline bool command_line_parser::run( int argc, char * argv[], callback_type cb, void * user ) const
{
	typedef struct ::option getopt_option;

	std::vector<option_type>::const_iterator opt;
	std::vector<getopt_option> long_opts;
	std::ostringstream short_opts;
	int next = 0, long_index;
	bool ok = true;

	// Reset the getopt index
	optind = 1;

	// Build the getopt option array
	for ( opt = _options.begin(); opt != _options.end(); ++opt )
	{
		option_type::set_char_type::const_iterator c;
		option_type::set_string_type::const_iterator name;

		for ( c = opt->index().keys().begin(); c != opt->index().keys().end(); ++c )
		{
			short_opts << char( *c );
			if ( !opt->arg().empty() )
			{
				short_opts << ( ( opt->arg().is_optional() ) ? "::" : ":" );
			}
		}

		for ( name = opt->index().names().begin(); name != opt->index().names().end(); ++name )
		{
			getopt_option lo;
			lo.name    = name->c_str();
			lo.has_arg = !opt->arg().empty()
				         ? ( opt->arg().is_optional()
				           ? (optional_argument)
				           : (required_argument))
				         : (no_argument);
			lo.flag    = 0;
			lo.val     = 0x80 + long_opts.size();
			long_opts.push_back( lo );
		}
	}

	// Past-the-end option
	{
		getopt_option lo = { 0, 0, 0, 0 };
		long_opts.push_back( lo );
	}

	try
	{
		while ( ok && next != -1 )
		{
			long_index = -1;

			next = getopt_long( argc, argv,
					            short_opts.str().c_str(),
					            &long_opts.front(),
					            &long_index );

			if ( next != -1 )
			{
				if ( long_index != -1 )
				{
					ok = cb( 0, long_opts[long_index].name, optarg, user );
				}
				else
				{
					ok = cb( next, 0, optarg, user );
				}
			}
		}

		for ( int i = optind; ok && i < argc; ++i )
		{
			ok = cb( 0, 0, argv[i], user );
		}
	}
	catch ( const error & e )
	{
		std::cerr << argv[0] << ": " << e.what() << std::endl;
		ok = false;
	}
	return ok;
}

inline bool command_line_parser::default_parser( char c, const char * name, const char * arg ) const
{
	std::vector<option_type>::const_iterator opt;

	if ( c || name )
	{
		if ( c )
		{
			opt = std::find_if( _options.begin(), _options.end(), option_has_key( c ) );
		}
		else if ( name )
		{
			opt = std::find_if( _options.begin(), _options.end(), option_has_name( name ) );
		}

		if ( opt != _options.end() && opt->arg().callback() )
		{
			callback_type call = opt->arg().callback();
			void * user = opt->arg().userdata();

			return call( c, name, arg, user );
		}
	}
	else if ( _callback_arg )
	{
		return _callback_arg( c, name, arg, _userdata_arg );
	}
	return true;
}

inline bool command_line_parser::default_parser( char c, const char * name, const char * arg, void * user )
{
	return static_cast<const command_line_parser *>( user )->default_parser( c, name, arg );
}

inline bool command_line_parser::debug_parser( char c, const char * name, const char * arg, void * user )
{
	if ( c )
	{
		if ( std::isprint( c ) )
		{
			std::cout << "'" << c << "'";
		}
		else
		{
			std::cout << "[" << int( c ) << "]";
		}
	}

	if ( name )
	{
		std::cout << '"' << name << '"';
	}

	if ( arg )
	{
		if ( c || name )
		{
			std::cout << " = ";
		}
		std::cout << '"' << arg << '"';
	}
	std::cout << std::endl;
	return true;
}

////////////////////////////////////////////////////////////////////////////////

invalid_value_error::invalid_value_error( char key, const char * name ) :
	std::runtime_error( "invalid option value" ),
	_key( key ),
	_name( name )
{
	std::ostringstream os;
	os << "in option ";
	if ( _key )
	{
		os << '\'' << _key << '\'';
	}
	else if ( _name )
	{
		os << '"' << _name << '"';
	}
	os << ": invalid option value";
	_what = os.str();
}

const char * invalid_value_error::what() const throw ()
{
	return _what.c_str();
}

////////////////////////////////////////////////////////////////////////////////

inline wrapbuf::wrapbuf( std::streambuf * s, size_type w ) :
	_sbuf( s ),
	_indent( 0 ),
	_width( w ),
	_count( 0 ),
	_first( -1 )
{
}

inline wrapbuf::wrapbuf( std::streambuf * s, size_type indentw, size_type w ) :
	_sbuf( s ),
	_indent( indentw ),
	_width( w ),
	_count( 0 ),
	_first( -1 )
{
}

inline wrapbuf::~wrapbuf()
{
	overflow( '\n' );
}

inline void wrapbuf::indent( size_type indentw )
{
	_indent = indentw;
}

inline wrapbuf::size_type wrapbuf::indent() const
{
	return _indent;
}

inline void wrapbuf::width( size_type w )
{
	_width = w;
}

inline wrapbuf::size_type wrapbuf::width() const
{
	return _width;
}

inline void wrapbuf::first( int_type w )
{
	_first = w;
}

inline wrapbuf::int_type wrapbuf::first() const
{
	return _first;
}

inline wrapbuf::int_type wrapbuf::overflow( int_type c )
{
	if ( traits_type::eq_int_type( traits_type::eof(), c ) )
	{
		return traits_type::not_eof( c );
	}

	switch ( c )
	{
		case '\r':
		case '\n':
		{
			_buffer += c;
			_count = 0;
			_put_margin();
			int_type rc = _sbuf->sputn( _buffer.c_str(), _buffer.size() );
			_buffer.clear();
			return rc;
		}
		case '\a':
			return _sbuf->sputc( c );
		case '\t':
			_buffer += c;
			_count += _tab_width - _count % _tab_width;
			return c;
		default:
			if ( _count >= _width )
			{
				size_type wpos = _buffer.find_last_of( " \t" );
				_put_margin();
				if ( wpos != string_type::npos )
				{
					_sbuf->sputn( _buffer.c_str(), wpos );
					_count = _buffer.size() - wpos - 1;
					_buffer = string_type( _buffer, wpos + 1 );
				}
				else
				{
					_sbuf->sputn( _buffer.c_str(), _buffer.size() );
					_buffer.clear();
					_count = 0;
				}
				_sbuf->sputc( '\n' );
			}
			_buffer += c;
			++_count;
			return c;
	}
}

inline void wrapbuf::_put_margin()
{
	if ( _first < 0 )
	{
		_sbuf->sputn( string_type( _indent, ' ' ).c_str(), _indent );
	}
	else
	{
		_sbuf->sputn( string_type( _first, ' ' ).c_str(), _first );
		_first = -1;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline wrapstream::wrapstream( std::ostream & os, size_type w ) :
	std::ostream( &_buf ),
	_buf( os.rdbuf(), w )
{
}

inline wrapstream::wrapstream( std::ostream & os, size_type indentw, size_type w ) :
	std::ostream( &_buf ),
	_buf( os.rdbuf(), indentw, w )
{
}

inline wrapstream & wrapstream::indent( size_type indentw )
{
	_buf.indent( indentw );
	return *this;
}

inline wrapstream::size_type wrapstream::indent() const
{
	return _buf.indent();
}

inline wrapstream & wrapstream::wrap( size_type w )
{
	_buf.width( w );
	return *this;
}

inline wrapstream & wrapstream::wrap( size_type indentw, size_type w )
{
	_buf.indent( indentw );
	_buf.width( w );
	return *this;
}

inline wrapstream & wrapstream::first( size_type w )
{
	_buf.first( w );
	return *this;
}

}

#endif
