/* -*- c++ -*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SVG_HPP
#define SVG_HPP

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <limits>
#include <cmath>

namespace svg
{

/*
	Class: optional

	Optional value.
*/
template<typename T>
class optional
{
public:
	typedef T value_type;

	optional() : _x(), _d( false ) {}
	optional( const value_type & x ) : _x( x ), _d( true ) {}

	optional & operator = ( const value_type & x ) { _x = x; _d = true; return *this; }
	operator value_type() const { return _x; }
	bool empty() const { return !_d; }
	bool operator ! () const { return !_d; }

private:
	value_type _x;
	bool _d;
};

/*
	Class: serializable

	Base class for every object that have a text output.
*/
class serializable
{
public:
	/*
		Constructor: serializable
	*/
	serializable();

	/*
		Method: str

		Export the object as a string.
	*/
	std::string str() const;

	/*
		Method: write

		Export the object into a ostream.
	*/
	virtual void write( std::ostream & os ) const = 0;
};

/*
	Class: xml_element

	Base class for XML elements.
*/
class xml_element : public serializable
{
public:
	virtual ~xml_element() {}

	/*
		Method: name

		Return the name of the element.
	*/
	virtual std::string name() const = 0;
};

/*
	Class: element<D>

	Base class for elements that have an id attribute.
*/
template<class D>
class element : public xml_element
{
public:
	typedef D derived_type;

	/*
		Constructor: element
	*/
	element( const std::string & id = "" );
	element( const element & e );

	/*
		Method: id

		Return the "id" attribute.
	*/
	const std::string & id() const;

	/*
		Method: title

		Return the title of this element.
	*/
	const std::string & title() const;

	/*
		Method: desc

		Return the description of this element.
	*/
	const std::string & desc() const;

	/*
		Method: id

		Set the "id" attribute.
	*/
	derived_type & id( const std::string & id );

	/*
		Method: title

		Set the title of this element.
	*/
	derived_type & title( const std::string & title );

	/*
		Method: desc

		Set the description of this element.
	*/
	derived_type & desc( const std::string & desc );

	/*
		Method: attr

		Return a custom attribute.
	*/
	std::string attr( const std::string & key ) const;

	/*
		Method: attrs

		Return all the attributes.
	*/
	const std::map<std::string, std::string> & attrs() const;

	/*
		Method: attr

		Set a custom attribute.
	*/
	derived_type & attr( const std::string & key, const std::string & value );
	derived_type & attr( const std::string & key, int value );
	derived_type & attr( const std::string & key, double value );

private:
	std::string _id, _title, _desc;
	std::map<std::string, std::string> _attr;
};

/*
	Class: shape<D>

	Base class for shape elements.
*/
template<class D>
class shape : public element<D>
{
public:
	typedef D derived_type;

	/*
		Constructor: shape
	*/
	shape( const std::string & id = "", const std::string & cls = "" );
	shape( const shape & e );

	/*
		Method: class_name

		Return the "class" attribute.
	*/
	const std::string & class_name() const;

	/*
		Method: style

		Return the "style" attribute.
	*/
	const std::string & style() const;

	/*
		Method: class_name

		Set the "class" attribute.
	*/
	derived_type & class_name( const std::string & cls );

	/*
		Method: style

		Set the "style" attribute.
	*/
	derived_type & style( const std::string & s );

private:
	std::string _class, _style;
};

/*
	Class: line

	A line.
*/
class line : public shape<line>
{
public:
	/*
		Constructor: line
	*/
	line( const std::string & id, double x1, double y1, double x2, double y2 );
	line( double x1, double y1, double x2, double y2 );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: x1

		Return the x-coordinate of the source point.
	*/
	double x1() const;

	/*
		Method: y1

		Return the y-coordinate of the source point.
	*/
	double y1() const;

	/*
		Method: x2

		Return the x-coordinate of the target point.
	*/
	double x2() const;

	/*
		Method: y2

		Return the y-coordinate of the target point.
	*/
	double y2() const;

	/*
		Method: x1

		Set the x-coordinate of the source point.
	*/
	line & x1( double x );

	/*
		Method: y1

		Set the y-coordinate of the source point.
	*/
	line & y1( double y );

	/*
		Method: x2

		Set the x-coordinate of the target point.
	*/
	line & x2( double x );

	/*
		Method: y2

		Set the y-coordinate of the target point.
	*/
	line & y2( double y );

	/*
		Method: write

		Output the rectangle.
	*/
	void write( std::ostream & os ) const;

private:
	double _x1, _y1, _x2, _y2;
};

/*
	Class: circle

	A circle.
*/
class circle : public shape<circle>
{
public:
	/*
		Constructor: circle
	*/
	circle( const std::string & id, double x, double y, double radius );
	circle( const std::string & id, double radius );
	circle( double x, double y, double radius );
	circle( const circle & e );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: cx

		Return the x-coordinate of the center of the circle.
	*/
	optional<double> cx() const;

	/*
		Method: cy

		Return the y-coordinate of the center of the circle.
	*/
	optional<double> cy() const;

	/*
		Method: r

		Return the radius of the circle.
	*/
	double r() const;

	/*
		Method: cx

		Set the x-coordinate of the center of the circle.
	*/
	circle & cx( double x );

	/*
		Method: cy

		Set the y-coordinate of the center of the circle.
	*/
	circle & cy( double y );

	/*
		Method: r

		Set the radius of the circle.
	*/
	circle & r( double radius );

	/*
		Method: write

		Output the circle.
	*/
	void write( std::ostream & os ) const;

private:
	optional<double> _cx, _cy;
	double _r;
};

/*
	Class: rect

	A rectangle.
*/
class rect : public shape<rect>
{
public:
	/*
		Constructor: rect
	*/
	rect( const std::string & id, double x, double y, double w, double h );
	rect( const std::string & id, double w, double h );
	rect( double x, double y, double w, double h );
	rect( const rect & e );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: x

		Return the x-upper-left coordinate of the rectangle.
	*/
	optional<double> x() const;

	/*
		Method: y

		Return the y-upper-left coordinate of the rectangle.
	*/
	optional<double> y() const;

	/*
		Method: width

		Return the width of the rectangle.
	*/
	double width() const;

	/*
		Method: height

		Return the height of the rectangle.
	*/
	double height() const;

	/*
		Method: x

		Set the x-upper-left coordinate of the rectangle.
	*/
	rect & x( double x );

	/*
		Method: y

		Set the y-upper-left coordinate of the rectangle.
	*/
	rect & y( double y );

	/*
		Method: width

		Set the width of the rectangle.
	*/
	rect & width( double w );

	/*
		Method: height

		Set the height of the rectangle.
	*/
	rect & height( double h );

	/*
		Method: write

		Output the rectangle.
	*/
	void write( std::ostream & os ) const;

private:
	optional<double> _x, _y;
	double _width, _height;
};

/*
	Class: path

	A path.
*/
class path : public shape<path>
{
public:
	enum abs_or_rel
	{
		absolute = 0,
		relative = 1
	};

	/*
		Constructor: path
	*/
	path( const std::string & id = "" );
	path( const path & e );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: d

		Return the path data.
	*/
	std::string d() const;

	/*
		Method: move_to

		Move to a given point.
	*/
	path & move_to( double x, double y, abs_or_rel aor = absolute );

	/*
		Method: line_to

		Set a line to a given point.
	*/
	path & line_to( double x, double y, abs_or_rel aor = absolute );

	/*
		Method: horizontal_to

		Set an horizontal line to a given x-coordinate.
	*/
	path & horizontal_to( double x, abs_or_rel aor = absolute );

	/*
		Method: vertical_to

		Set a vertical line to a given y-coordinate.
	*/
	path & vertical_to( double y, abs_or_rel aor = absolute );

	/*
		Method: curve_to

		Set a cubic Bézier curve from the current point to (x,y) using (x1,y1)
		as the control point at the beginning of the curve and (x2,y2) as the
		control point at the end of the curve.
	*/
	path & curve_to( double x1, double y1, double x2, double y2, double x, double y, abs_or_rel aor = absolute );

	/*
		Method: smooth_to

		Set a cubic Bézier curve from the current point to (x,y).
	*/
	path & smooth_to( double x2, double y2, double x, double y, abs_or_rel aor = absolute );

	/*
		Method: quadratic_to

		Set a quadratic Bézier curve from the current point to (x,y) using
		(x1,y1) as the control point.
	*/
	path & quadratic_to( double x1, double y1, double x, double y, abs_or_rel aor = absolute );

	/*
		Method: smooth_quadratic_to

		Set a quadratic Bézier curve from the current point to (x,y).
	*/
	path & smooth_quadratic_to( double x, double y, abs_or_rel aor = absolute );

	/*
		Method: elliptical_arc

		Set an elliptical arc from the current point to (x, y).
	*/
	path & elliptical_arc( double rx, double ry, double xrot, bool large_arc, bool sweep, double x, double y, abs_or_rel aor = absolute );

	/*
		Method: close

		Close the path.
	*/
	path & close();

	/*
		Method: write

		Output the path.
	*/
	void write( std::ostream & os ) const;

private:
	std::ostringstream _d;
};

/*
	Class: text

	A text.
*/
class text : public shape<text>
{
public:
	/*
		Constructor: text
	*/
	text( const std::string & id = "", const std::string & s = "" );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: x

		Return the x-coordinate of the text.
	*/
	optional<double> x() const;

	/*
		Method: y

		Return the y-coordinate of the text.
	*/
	optional<double> y() const;

	/*
		Method: dx

		Return the dx-coordinate of the text.
	*/
	optional<double> dx() const;

	/*
		Method: dy

		Return the dy-coordinate of the text.
	*/
	optional<double> dy() const;

	/*
		Method: text_anchor

		Return the text-anchor attribute.
	*/
	const std::string & text_anchor() const;

	/*
		Method: str

		Return the string.
	*/
	const std::string & str() const;

	/*
		Method: x

		Set the x-coordinate of the text.
	*/
	text & x( double x );

	/*
		Method: y

		Set the y-coordinate of the text.
	*/
	text & y( double y );

	/*
		Method: dx

		Set the dx-coordinate of the text.
	*/
	text & dx( double dx );

	/*
		Method: dy

		Set the dy-coordinate of the text.
	*/
	text & dy( double dy );

	/*
		Method: text_anchor

		Set the text-anchor attribute.
	*/
	text & text_anchor( const std::string & ta );

	/*
		Method: str

		Set the string.
	*/
	text & str( const std::string & s );

	/*
		Method: write

		Output the group.
	*/
	void write( std::ostream & os ) const;

private:
	optional<double> _x, _y, _dx, _dy;
	std::string _text_anchor, _str;
};

/*
	Class: g

	A group of elements.
*/
class g : public shape<g>
{
public:
	/*
		Constructor: path
	*/
	g( const std::string & id = "" );
	~g();

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: append

		Append an element to the group.
	*/
	template<class D>
	g & append( const element<D> & e );

	/*
		Method: write

		Output the group.
	*/
	void write( std::ostream & os ) const;

private:
	std::vector<xml_element *> _children;
};

/*
	Class: use

	A reference to a defined element.
*/
class use : public shape<use>
{
public:
	/*
		Constructor: use
	*/
	use( const std::string & id, const std::string & link );
	use( const std::string & link );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: href

		Return the reference.
	*/
	const std::string & href() const;

	/*
		Method: x

		Return the x-upper-left coordinate of the bounding box.
	*/
	optional<double> x() const;

	/*
		Method: y

		Return the y-upper-left coordinate of the bounding box.
	*/
	optional<double> y() const;

	/*
		Method: width

		Return the width of the bounding box.
	*/
	optional<double> width() const;

	/*
		Method: height

		Return the height of the bounding box.
	*/
	optional<double> height() const;

	/*
		Method: href

		Set the reference.
	*/
	use & href( const std::string & link );

	/*
		Method: x

		Set the x-upper-left coordinate of the bounding box.
	*/
	use & x( double x );

	/*
		Method: y

		Set the y-upper-left coordinate of the bounding box.
	*/
	use & y( double y );

	/*
		Method: width

		Set the width of the bounding box.
	*/
	use & width( double w );

	/*
		Method: height

		Set the height of the bounding box.
	*/
	use & height( double h );

	/*
		Method: write

		Output the element.
	*/
	void write( std::ostream & os ) const;

private:
	std::string _href;
	optional<double> _x, _y, _width, _height;
};

/*
	Class: animation

	Base class for animation elements.
*/
template<class D>
class animation : public element<D>
{
public:
	typedef D derived_type;

	/*
		Constructor: animation
	*/
	animation( const std::string & id = "" );
	animation( const animation & e );

	/*
		Method: begin

		Return the begin time.
	*/
	optional<double> begin() const;

	/*
		Method: dur

		Return the duration.
	*/
	optional<double> dur() const;

	/*
		Method: end

		Return the end time.
	*/
	optional<double> end() const;

	/*
		Method: min

		Return the minimum duration.
	*/
	optional<double> min() const;

	/*
		Method: max

		Return the maximum duration.
	*/
	optional<double> max() const;

	/*
		Method: restart

		Return the "restart" attribute.
	*/
	const std::string & restart() const;

	/*
		Method: repeat_count

		Return the "repeatCount" attribute.
	*/
	optional<double> repeat_count() const;

	/*
		Method: repeat_dur

		Return the "repeatDur" attribute.
	*/
	optional<double> repeat_dur() const;

	/*
		Method: fill

		Return the "fill" attribute.
	*/
	const std::string & fill() const;

	/*
		Method: begin

		Set the begin time.
	*/
	derived_type & begin( double t );

	/*
		Method: dur

		Set the duration.
	*/
	derived_type & dur( double t );

	/*
		Method: end

		Set the end time.
	*/
	derived_type & end( double t );

	/*
		Method: min

		Return the minimum duration.
	*/
	derived_type & min( double t );

	/*
		Method: max

		Return the maximum duration.
	*/
	derived_type & max( double t );

	/*
		Method: restart

		Return the "restart" attribute.
	*/
	derived_type & restart( const std::string & r );

	/*
		Method: repeat_count

		Return the "repeatCount" attribute.
	*/
	derived_type & repeat_count( double n );

	/*
		Method: repeat_dur

		Return the "repeatDur" attribute.
	*/
	derived_type & repeat_dur( double t );

	/*
		Method: fill

		Set the "fill" attribute.
	*/
	derived_type & fill( const std::string & f );

private:
	optional<double> _begin, _dur, _end, _min, _max;
	std::string _restart;
	optional<double> _repeat_count, _repeat_dur;
	std::string _fill;
};

/*
	Class: set

	Set an attribute at a given time during a given length.
*/
class set : public animation<set>
{
public:
	/*
		Constructor: set
	*/
	set( const std::string & id, const std::string & link );
	set( const std::string & link = "" );
	set( const set & e );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: href

		Return a reference to an element.
	*/
	const std::string & href() const;

	/*
		Method: attribute_name

		Return the attribute name.
	*/
	const std::string & attribute_name() const;

	/*
		Method: attribute_type

		Return the attribute type (XML, CSS, auto).
	*/
	const std::string & attribute_type() const;

	/*
		Method: to

		Return the value to set.
	*/
	const std::string & to() const;

	/*
		Method: href

		Set a reference to an element.
	*/
	set & href( const std::string & link );

	/*
		Method: attribute_name

		Set the attribute name.
	*/
	set & attribute_name( const std::string & name );

	/*
		Method: attribute_type

		Set the attribute type (XML, CSS, auto).
	*/
	set & attribute_type( const std::string & type );

	/*
		Method: to

		Set the value to set.
	*/
	set & to( const std::string & value );
	set & to( int value );
	set & to( double value );

	/*
		Method: write

		Output the set element.
	*/
	void write( std::ostream & os ) const;

private:
	std::string _href, _attribute_name, _attribute_type, _to;
};

/*
	Class: animate_motion

	Moves a object.
*/
class animate_motion : public animation<animate_motion>
{
public:
	/*
		Constructor: animate_motion
	*/
	animate_motion( const std::string & id, const std::string & link );
	animate_motion( const std::string & link = "" );
	animate_motion( const animate_motion & e );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: href

		Return a reference to an element.
	*/
	const std::string & href() const;

	/*
		Method: calc_mode

		Return the "calcMode" attribute.
	*/
	const std::string & calc_mode() const;

	/*
		Method: mpath

		Return a reference to a path.
	*/
	const std::string & mpath() const;

	/*
		Method: href

		Set a reference to an element.
	*/
	animate_motion & href( const std::string & link );

	/*
		Method: calc_mode

		Set the "calcMode" attribute.
	*/
	animate_motion & calc_mode( const std::string & cm );

	/*
		Method: mpath

		Set a reference to a path.
	*/
	animate_motion & mpath( const std::string & link );

	/*
		Method: write

		Output the set element.
	*/
	void write( std::ostream & os ) const;

private:
	std::string _href, _calc_mode, _mpath;
};

/*
	Class: animate_transform

	Transforms a object.
*/
class animate_transform : public animation<animate_transform>
{
public:
	/*
		Constructor: animate_transform
	*/
	animate_transform( const std::string & id, const std::string & link );
	animate_transform( const std::string & link = "" );

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	/*
		Method: href

		Return a reference to an element.
	*/
	const std::string & href() const;

	/*
		Method: attribute_name

		Return the attribute name.
	*/
	const std::string & attribute_name() const;

	/*
		Method: attribute_type

		Return the attribute type (XML, CSS, auto).
	*/
	const std::string & attribute_type() const;

	/*
		Method: from

		Return the "from" attribute.
	*/
	const std::string & from() const;

	/*
		Method: to

		Return the "to" attribute.
	*/
	const std::string & to() const;

	/*
		Method: type

		Return the "type" attribute.
	*/
	const std::string & type() const;

	/*
		Method: href

		Set a reference to an element.
	*/
	animate_transform & href( const std::string & link );

	/*
		Method: attribute_name

		Set the attribute name.
	*/
	animate_transform & attribute_name( const std::string & name );

	/*
		Method: attribute_type

		Set the attribute type (XML, CSS, auto).
	*/
	animate_transform & attribute_type( const std::string & type );

	/*
		Method: from

		Set the "from" attribute.
	*/
	animate_transform & from( const std::string & f );

	/*
		Method: to

		Set the "to" attribute.
	*/
	animate_transform & to( const std::string & t );

	/*
		Method: type

		Set the "type" attribute.
	*/
	animate_transform & type( const std::string & t );

	/*
		Method: write

		Output the set element.
	*/
	void write( std::ostream & os ) const;

private:
	std::string _href, _attribute_name, _attribute_type, _from, _to, _type;
};

/*
	Class: document
*/
class document : public element<document>
{
public:
	document( double w = 100, double h = 100 );
	document( const document & e );
	~document();

	/*
		Method: name

		Return the name of the element.
	*/
	std::string name() const;

	double width() const;
	double height() const;
	std::string view_box() const;

	document & width( double w );
	document & height( double h );
	document & view_box( double x, double y, double w, double h );

	template<class D>
	void append( const element<D> & e );

	template<class D>
	void define( const element<D> & e );

	document & css( const std::string & selector, const std::string & key, const std::string & value );
	void write( std::ostream & os ) const;

private:
	double _width, _height;
	double _vbx, _vby, _vbw, _vbh;
	std::ostringstream _oss;
	std::map<std::string, std::map<std::string, std::string> > _css;
	std::vector<xml_element *> _defs;
};

template<class D>
document & operator << ( document & doc, const element<D> & el );

////////////////////////////////////////////////////////////////////////////////

inline serializable::serializable()
{
}

inline std::string serializable::str() const
{
	std::ostringstream oss;
	write( oss );
	return oss.str();
}

////////////////////////////////////////////////////////////////////////////////

template<class D>
inline element<D>::element( const std::string & id ) :
	_id( id )
{
}

template<class D>
inline element<D>::element( const element & e ) :
	_id( e._id ), _attr( e._attr )
{
}

template<class D>
inline const std::string & element<D>::id() const
{
	return _id;
}

template<class D>
inline const std::string & element<D>::title() const
{
	return _title;
}

template<class D>
inline const std::string & element<D>::desc() const
{
	return _desc;
}

template<class D>
inline std::string element<D>::attr( const std::string & key ) const
{
	return _attr.at( key );
}

template<class D>
inline const std::map<std::string, std::string> & element<D>::attrs() const
{
	return _attr;
}

template<class D>
inline typename element<D>::derived_type & element<D>::id( const std::string & id )
{
	_id = id;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename element<D>::derived_type & element<D>::title( const std::string & title )
{
	_title = title;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename element<D>::derived_type & element<D>::desc( const std::string & desc )
{
	_desc = desc;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename element<D>::derived_type & element<D>::attr( const std::string & key, const std::string & value )
{
	_attr[ key ] = value;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename element<D>::derived_type & element<D>::attr( const std::string & key, int value )
{
	std::ostringstream oss;
	oss << value;
	_attr[ key ] = oss.str();
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename element<D>::derived_type & element<D>::attr( const std::string & key, double value )
{
	std::ostringstream oss;
	oss << value;
	_attr[ key ] = oss.str();
	return static_cast<derived_type &>( *this );
}

////////////////////////////////////////////////////////////////////////////////

template<class D>
inline shape<D>::shape( const std::string & id, const std::string & cls ) :
	element<D>( id ), _class( cls )
{
}

template<class D>
inline shape<D>::shape( const shape & e ) :
	element<D>( e ), _class( e._class ), _style( e._style )
{
}

template<class D>
inline const std::string & shape<D>::class_name() const
{
	return _class;
}

template<class D>
inline const std::string & shape<D>::style() const
{
	return _style;
}

template<class D>
inline typename shape<D>::derived_type & shape<D>::class_name( const std::string & cls )
{
	_class = cls;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename shape<D>::derived_type & shape<D>::style( const std::string & s )
{
	_style = s;
	return static_cast<derived_type &>( *this );
}

////////////////////////////////////////////////////////////////////////////////

inline line::line( const std::string & id, double x1, double y1, double x2, double y2 ) :
	shape<line>( id ), _x1( x1 ), _y1( y1 ), _x2( x2 ), _y2( y2 )
{
}

inline line::line( double x1, double y1, double x2, double y2 ) :
	shape<line>(), _x1( x1 ), _y1( y1 ), _x2( x2 ), _y2( y2 )
{
}

inline std::string line::name() const
{
	return "line";
}

inline double line::x1() const
{
	return _x1;
}

inline double line::y1() const
{
	return _y1;
}

inline double line::x2() const
{
	return _x2;
}

inline double line::y2() const
{
	return _y2;
}

inline line & line::x1( double x )
{
	_x1 = x;
	return *this;
}

inline line & line::y1( double y )
{
	_y1 = y;
	return *this;
}

inline line & line::x2( double x )
{
	_x2 = x;
	return *this;
}

inline line & line::y2( double y )
{
	_y2 = y;
	return *this;
}

inline void line::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<" << name();
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !class_name().empty() )
	{
		os << " class=\"" << class_name() << "\"";
	}
	if ( !style().empty() )
	{
		os << " style=\"" << style() << "\"";
	}
	os
		<< " x1=\"" << x1() << "\""
		<< " y1=\"" << y1() << "\""
		<< " x2=\"" << x2() << "\""
		<< " y2=\"" << y2() << "\"";
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	if ( !title().empty() || !desc().empty() )
	{
		os << ">";
		if ( !title().empty() )
		{
			os << "<title>" << title() << "</title>";
		}
		if ( !desc().empty() )
		{
			os << "<desc>" << desc() << "</desc>";
		}
		os << "</" << name() << ">";
	}
	else
	{
		os << "/>";
	}
}

////////////////////////////////////////////////////////////////////////////////

inline circle::circle( const std::string & id, double x, double y, double radius ) :
	shape<circle>( id ), _cx( x ), _cy( y ), _r( radius )
{
}

inline circle::circle( const std::string & id, double radius ) :
	shape<circle>( id ), _r( radius )
{
}

inline circle::circle( double x, double y, double radius ) :
	shape<circle>(), _cx( x ), _cy( y ), _r( radius )
{
}

inline circle::circle( const circle & e ) :
	shape<circle>( e ), _cx( e._cx ), _cy( e._cy ), _r( e._r )
{
}

inline std::string circle::name() const
{
	return "circle";
}

inline optional<double> circle::cx() const
{
	return _cx;
}

inline optional<double> circle::cy() const
{
	return _cy;
}

inline double circle::r() const
{
	return _r;
}

inline circle & circle::cx( double x )
{
	_cx = x;
	return *this;
}

inline circle & circle::cy( double y )
{
	_cy = y;
	return *this;
}

inline circle & circle::r( double radius )
{
	_r = radius;
	return *this;
}

inline void circle::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<circle";
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !class_name().empty() )
	{
		os << " class=\"" << class_name() << "\"";
	}
	if ( !style().empty() )
	{
		os << " style=\"" << style() << "\"";
	}
	if ( !cx().empty() )
	{
		os << " cx=\"" << cx() << "\"";
	}
	if ( !cy().empty() )
	{
		os << " cy=\"" << cy() << "\"";
	}
	os << " r=\"" << r() << "\"";
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	if ( !title().empty() || !desc().empty() )
	{
		os << ">";
		if ( !title().empty() )
		{
			os << "<title>" << title() << "</title>";
		}
		if ( !desc().empty() )
		{
			os << "<desc>" << desc() << "</desc>";
		}
		os << "</" << name() << ">";
	}
	else
	{
		os << "/>";
	}
}

////////////////////////////////////////////////////////////////////////////////

inline rect::rect( const std::string & id, double x, double y, double w, double h ) :
	shape<rect>( id ), _x( x ), _y( y ), _width( w ), _height( h )
{
}

inline rect::rect( const std::string & id, double w, double h ) :
	shape<rect>( id ), _width( w ), _height( h )
{
}

inline rect::rect( double x, double y, double w, double h ) :
	shape<rect>(), _x( x ), _y( y ), _width( w ), _height( h )
{
}

inline rect::rect( const rect & e ) :
	shape<rect>( e ), _x( e._x ), _y( e._y ), _width( e._width ), _height( e._height )
{
}

inline std::string rect::name() const
{
	return "rect";
}

inline optional<double> rect::x() const
{
	return _x;
}

inline optional<double> rect::y() const
{
	return _y;
}

inline double rect::width() const
{
	return _width;
}

inline double rect::height() const
{
	return _height;
}

inline rect & rect::x( double x )
{
	_x = x;
	return *this;
}

inline rect & rect::y( double y )
{
	_y = y;
	return *this;
}

inline rect & rect::width( double w )
{
	_width = w;
	return *this;
}

inline rect & rect::height( double h )
{
	_height = h;
	return *this;
}

inline void rect::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<" << name();
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !class_name().empty() )
	{
		os << " class=\"" << class_name() << "\"";
	}
	if ( !style().empty() )
	{
		os << " style=\"" << style() << "\"";
	}
	if ( !x().empty() )
	{
		os << " x=\"" << x() << "\"";
	}
	if ( !y().empty() )
	{
		os << " y=\"" << y() << "\"";
	}
	os
		<< " width=\"" << width() << "\""
		<< " height=\"" << height() << "\"";
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	if ( !title().empty() || !desc().empty() )
	{
		os << ">";
		if ( !title().empty() )
		{
			os << "<title>" << title() << "</title>";
		}
		if ( !desc().empty() )
		{
			os << "<desc>" << desc() << "</desc>";
		}
		os << "</" << name() << ">";
	}
	else
	{
		os << "/>";
	}
}

////////////////////////////////////////////////////////////////////////////////

inline path::path( const std::string & id ) :
	shape<path>( id )
{
}

inline path::path( const path & e ) :
	shape<path>( e ),
	_d( e._d.str() )
{
}

inline std::string path::name() const
{
	return "path";
}

inline std::string path::d() const
{
	return _d.str();
}

inline path & path::move_to( double x, double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'M' : 'm' ) << ' ' << x << ' ' << y;
	return *this;
}

inline path & path::line_to( double x, double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'L' : 'l' ) << ' ' << x << ' ' << y;
	return *this;
}

inline path & path::horizontal_to( double x, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'H' : 'h' ) << ' ' << x;
	return *this;
}

inline path & path::vertical_to( double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'V' : 'v' ) << ' ' << y;
	return *this;
}

inline path & path::curve_to( double x1, double y1, double x2, double y2, double x, double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'C' : 'c' ) << ' '
		<< x1 << ' ' << y1 << ' '
		<< x2 << ' ' << y2 << ' '
		<< x << ' ' << y;
	return *this;
}

inline path & path::smooth_to( double x2, double y2, double x, double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'S' : 's' ) << ' '
		<< x2 << ' ' << y2 << ' '
		<< x << ' ' << y;
	return *this;
}

inline path & path::quadratic_to( double x1, double y1, double x, double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'Q' : 'q' ) << ' '
		<< x1 << ' ' << y1 << ' '
		<< x << ' ' << y;
	return *this;
}

inline path & path::smooth_quadratic_to( double x, double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'T' : 't' ) << ' '
		<< x << ' ' << y;
	return *this;
}

inline path & path::elliptical_arc( double rx, double ry, double xrot, bool large_arc, bool sweep, double x, double y, abs_or_rel aor )
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << ( aor == absolute ? 'A' : 'a' ) << ' '
		<< rx << ' ' << ry << ' ' << xrot << ' '
		<< large_arc << ' ' << sweep << ' '
		<< x << ' ' << y;
	return *this;
}

inline path & path::close()
{
	if ( !_d.str().empty() )
		_d << ' ';
	_d << 'Z';
	return *this;
}

inline void path::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<" << name();
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !class_name().empty() )
	{
		os << " class=\"" << class_name() << "\"";
	}
	if ( !style().empty() )
	{
		os << " style=\"" << style() << "\"";
	}
	os << " d=\"" << d() << "\"";
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	if ( !title().empty() || !desc().empty() )
	{
		os << ">";
		if ( !title().empty() )
		{
			os << "<title>" << title() << "</title>";
		}
		if ( !desc().empty() )
		{
			os << "<desc>" << desc() << "</desc>";
		}
		os << "</" << name() << ">";
	}
	else
	{
		os << "/>";
	}
}

////////////////////////////////////////////////////////////////////////////////

inline text::text( const std::string & id, const std::string & s ) :
	shape<text>( id ), _str( s )
{
}

inline std::string text::name() const
{
	return "text";
}

inline optional<double> text::x() const
{
	return _x;
}

inline optional<double> text::y() const
{
	return _y;
}

inline optional<double> text::dx() const
{
	return _dx;
}

inline optional<double> text::dy() const
{
	return _dy;
}

inline const std::string & text::text_anchor() const
{
	return _text_anchor;
}

inline const std::string & text::str() const
{
	return _str;
}

inline text & text::x( double x )
{
	_x = x;
	return *this;
}

inline text & text::y( double y )
{
	_y = y;
	return *this;
}

inline text & text::dx( double dx )
{
	_dx = dx;
	return *this;
}

inline text & text::dy( double dy )
{
	_dy = dy;
	return *this;
}

inline text & text::text_anchor( const std::string & ta )
{
	_text_anchor = ta;
	return *this;
}

inline text & text::str( const std::string & s )
{
	_str = s;
	return *this;
}

inline void text::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<" << name();
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !class_name().empty() )
	{
		os << " class=\"" << class_name() << "\"";
	}
	if ( !style().empty() )
	{
		os << " style=\"" << style() << "\"";
	}
	if ( !x().empty() )
	{
		os << " x=\"" << x() << "\"";
	}
	if ( !y().empty() )
	{
		os << " y=\"" << y() << "\"";
	}
	if ( !dx().empty() )
	{
		os << " dx=\"" << dx() << "\"";
	}
	if ( !dy().empty() )
	{
		os << " dy=\"" << dy() << "\"";
	}
	if ( !text_anchor().empty() )
	{
		os << " text-anchor=\"" << text_anchor() << "\"";
	}
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	os
		<< ">"
		<< str()
		<< "</" << name() << ">";
}

////////////////////////////////////////////////////////////////////////////////

inline g::g( const std::string & id ) :
	shape<g>( id )
{
}

inline g::~g()
{
	for ( int i = 0; i < int( _children.size() ); ++i )
	{
		delete _children[i];
	}
}

inline std::string g::name() const
{
	return "g";
}

template<class D>
inline g & g::append( const element<D> & e )
{
	typedef typename element<D>::derived_type element_type;
	element_type * ptr = new element_type( static_cast<const element_type&>( e ) );
	_children.push_back( ptr );
	return *this;
}

inline void g::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<" << name();
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !class_name().empty() )
	{
		os << " class=\"" << class_name() << "\"";
	}
	if ( !style().empty() )
	{
		os << " style=\"" << style() << "\"";
	}
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	os << ">";
	if ( !title().empty() )
	{
		os << "<title>" << title() << "</title>";
	}
	if ( !desc().empty() )
	{
		os << "<desc>" << desc() << "</desc>";
	}
	for ( int i = 0; i < int( _children.size() ); ++i )
	{
		_children[i]->write( os );
	}
	os << "</" << name() << ">";
}

////////////////////////////////////////////////////////////////////////////////

inline use::use( const std::string & id, const std::string & link ) :
	shape<use>( id ), _href( link )
{
}

inline use::use( const std::string & link ) :
	shape<use>(), _href( link )
{
}

inline std::string use::name() const
{
	return "use";
}

inline const std::string & use::href() const
{
	return _href;
}

inline optional<double> use::x() const
{
	return _x;
}

inline optional<double> use::y() const
{
	return _y;
}

inline optional<double> use::width() const
{
	return _width;
}

inline optional<double> use::height() const
{
	return _height;
}

inline use & use::href( const std::string & link )
{
	_href = link;
	return *this;
}

inline use & use::x( double x )
{
	_x = x;
	return *this;
}

inline use & use::y( double y )
{
	_y = y;
	return *this;
}

inline use & use::width( double w )
{
	_width = w;
	return *this;
}

inline use & use::height( double h )
{
	_height = h;
	return *this;
}

inline void use::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<" << name();
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !class_name().empty() )
	{
		os << " class=\"" << class_name() << "\"";
	}
	if ( !style().empty() )
	{
		os << " style=\"" << style() << "\"";
	}
	os << " xlink:href=\"" << href() << "\"";
	if ( !x().empty() )
	{
		os << " x=\"" << x() << "\"";
	}
	if ( !y().empty() )
	{
		os << " y=\"" << y() << "\"";
	}
	if ( !width().empty() )
	{
		os << " width=\"" << width() << "\"";
	}
	if ( !height().empty() )
	{
		os << " height=\"" << height() << "\"";
	}
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	if ( !title().empty() || !desc().empty() )
	{
		os << ">";
		if ( !title().empty() )
		{
			os << "<title>" << title() << "</title>";
		}
		if ( !desc().empty() )
		{
			os << "<desc>" << desc() << "</desc>";
		}
		os << "</" << name() << ">";
	}
	else
	{
		os << "/>";
	}
}

////////////////////////////////////////////////////////////////////////////////

template<class D>
inline animation<D>::animation( const std::string & id ) :
	element<D>( id )
{
}

template<class D>
inline animation<D>::animation( const animation & e ) :
	element<D>( e ),
	_begin( e._begin ),
	_dur( e._dur ),
	_end( e._end ),
	_min( e._min ),
	_max( e._max ),
	_restart( e._restart ),
	_repeat_count( e._repeat_count ),
	_repeat_dur( e._repeat_dur ),
	_fill( e._fill )
{
}

template<class D>
inline optional<double> animation<D>::begin() const
{
	return _begin;
}

template<class D>
inline optional<double> animation<D>::dur() const
{
	return _dur;
}

template<class D>
inline optional<double> animation<D>::end() const
{
	return _end;
}

template<class D>
inline optional<double> animation<D>::min() const
{
	return _min;
}

template<class D>
inline optional<double> animation<D>::max() const
{
	return _max;
}

template<class D>
inline const std::string & animation<D>::restart() const
{
	return _restart;
}

template<class D>
inline optional<double> animation<D>::repeat_count() const
{
	return _repeat_count;
}

template<class D>
inline optional<double> animation<D>::repeat_dur() const
{
	return _repeat_dur;
}

template<class D>
inline const std::string & animation<D>::fill() const
{
	return _fill;
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::begin( double t )
{
	_begin = t;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::dur( double t )
{
	_dur = t;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::end( double t )
{
	_end = t;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::min( double t )
{
	_min = t;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::max( double t )
{
	_max = t;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::restart( const std::string & r )
{
	_restart = r;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::repeat_count( double n )
{
	_repeat_count = n;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::repeat_dur( double t )
{
	_repeat_dur = t;
	return static_cast<derived_type &>( *this );
}

template<class D>
inline typename animation<D>::derived_type & animation<D>::fill( const std::string & f )
{
	_fill = f;
	return static_cast<derived_type &>( *this );
}

////////////////////////////////////////////////////////////////////////////////

inline set::set( const std::string & id, const std::string & link ) :
	animation<set>( id ),
	_href( link )
{
}

inline set::set( const std::string & link ) :
	animation<set>(),
	_href( link )
{
}

inline set::set( const set & e ) :
	animation<set>( e ),
	_href( e._href ),
	_attribute_name( e._attribute_name ),
	_attribute_type( e._attribute_type ),
	_to( e._to )
{
}

inline std::string set::name() const
{
	return "set";
}

inline const std::string & set::href() const
{
	return _href;
}

inline const std::string & set::attribute_name() const
{
	return _attribute_name;
}

inline const std::string & set::attribute_type() const
{
	return _attribute_type;
}

inline const std::string & set::to() const
{
	return _to;
}

inline set & set::href( const std::string & link )
{
	_href = link;
	return *this;
}

inline set & set::attribute_name( const std::string & name )
{
	_attribute_name = name;
	return *this;
}

inline set & set::attribute_type( const std::string & type )
{
	_attribute_type = type;
	return *this;
}

inline set & set::to( const std::string & value )
{
	_to = value;
	return *this;
}

inline set & set::to( int value )
{
	std::ostringstream oss;
	oss << value;
	_to = oss.str();
	return *this;
}

inline set & set::to( double value )
{
	std::ostringstream oss;
	oss << value;
	_to = oss.str();
	return *this;
}

inline void set::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<set";
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !href().empty() )
	{
		os << " xlink:href=\"" << href() << "\"";
	}
	if ( !attribute_name().empty() )
	{
		os << " attributeName=\"" << attribute_name() << "\"";
	}
	if ( !attribute_type().empty() )
	{
		os << " attributeType=\"" << attribute_type() << "\"";
	}
	os << " to=\"" << to() << "\"";
	if ( !begin().empty() )
	{
		os << " begin=\"" << begin() << "s\"";
	}
	if ( !dur().empty() )
	{
		os << " dur=\"";
		if ( !std::isinf( double( dur() ) ) )
			os << dur() << "s";
		else
			os << "indefinite";
		os << "\"";
	}
	if ( !end().empty() )
	{
		os << " end=\"" << end() << "s\"";
	}
	if ( !min().empty() )
	{
		os << " min=\"" << min() << "s\"";
	}
	if ( !max().empty() )
	{
		os << " max=\"" << max() << "s\"";
	}
	if ( !restart().empty() )
	{
		os << " restart=\"" << restart() << "\"";
	}
	if ( !repeat_count().empty() )
	{
		os << " repeatCount=\"" << repeat_count() << "\"";
	}
	if ( !repeat_dur().empty() )
	{
		os << " repeatDur=\"" << repeat_dur() << "s\"";
	}
	if ( !fill().empty() )
	{
		os << " fill=\"" << fill() << "\"";
	}
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	os << "/>";
}

////////////////////////////////////////////////////////////////////////////////

inline animate_motion::animate_motion( const std::string & id, const std::string & link ) :
	animation<animate_motion>( id ),
	_href( link )
{
}

inline animate_motion::animate_motion( const std::string & link ) :
	animation<animate_motion>(),
	_href( link )
{
}

inline animate_motion::animate_motion( const animate_motion & e ) :
	animation<animate_motion>( e ),
	_href( e._href ),
	_calc_mode( e._calc_mode ),
	_mpath( e._mpath )
{
}

inline std::string animate_motion::name() const
{
	return "animateMotion";
}

inline const std::string & animate_motion::href() const
{
	return _href;
}

inline const std::string & animate_motion::calc_mode() const
{
	return _calc_mode;
}

inline const std::string & animate_motion::mpath() const
{
	return _mpath;
}

inline animate_motion & animate_motion::href( const std::string & link )
{
	_href = link;
	return *this;
}

inline animate_motion & animate_motion::calc_mode( const std::string & cm )
{
	_calc_mode = cm;
	return *this;
}

inline animate_motion & animate_motion::mpath( const std::string & link )
{
	_mpath = link;
	return *this;
}

inline void animate_motion::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<animateMotion";
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !href().empty() )
	{
		os << " xlink:href=\"" << href() << "\"";
	}
	if ( !begin().empty() )
	{
		os << " begin=\"" << begin() << "s\"";
	}
	if ( !dur().empty() )
	{
		os << " dur=\"";
		if ( !std::isinf( double( dur() ) ) )
			os << dur() << "s";
		else
			os << "indefinite";
		os << "\"";
	}
	if ( !end().empty() )
	{
		os << " end=\"" << end() << "s\"";
	}
	if ( !min().empty() )
	{
		os << " min=\"" << min() << "s\"";
	}
	if ( !max().empty() )
	{
		os << " max=\"" << max() << "s\"";
	}
	if ( !restart().empty() )
	{
		os << " restart=\"" << restart() << "\"";
	}
	if ( !repeat_count().empty() )
	{
		os << " repeatCount=\"" << repeat_count() << "\"";
	}
	if ( !repeat_dur().empty() )
	{
		os << " repeatDur=\"" << repeat_dur() << "s\"";
	}
	if ( !fill().empty() )
	{
		os << " fill=\"" << fill() << "\"";
	}
	if ( !calc_mode().empty() )
	{
		os << " calcMode=\"" << calc_mode() << "\"";
	}
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	if ( !mpath().empty() )
	{
		os << ">";
		os << "<mpath xlink:href=\"" << mpath() << "\"/>";
		os << "</animateMotion>";
	}
	else
	{
		os << "/>";
	}
}

////////////////////////////////////////////////////////////////////////////////

inline animate_transform::animate_transform( const std::string & id, const std::string & link ) :
	animation<animate_transform>( id ),
	_href( link )
{
}

inline animate_transform::animate_transform( const std::string & link ) :
	animation<animate_transform>(),
	_href( link )
{
}

inline std::string animate_transform::name() const
{
	return "animateTransform";
}

inline const std::string & animate_transform::href() const
{
	return _href;
}

inline const std::string & animate_transform::attribute_name() const
{
	return _attribute_name;
}

inline const std::string & animate_transform::attribute_type() const
{
	return _attribute_type;
}

inline const std::string & animate_transform::from() const
{
	return _from;
}

inline const std::string & animate_transform::to() const
{
	return _to;
}

inline const std::string & animate_transform::type() const
{
	return _type;
}

inline animate_transform & animate_transform::href( const std::string & link )
{
	_href = link;
	return *this;
}

inline animate_transform & animate_transform::attribute_name( const std::string & name )
{
	_attribute_name = name;
	return *this;
}

inline animate_transform & animate_transform::attribute_type( const std::string & type )
{
	_attribute_type = type;
	return *this;
}

inline animate_transform & animate_transform::from( const std::string & f )
{
	_from = f;
	return *this;
}

inline animate_transform & animate_transform::to( const std::string & t )
{
	_to = t;
	return *this;
}

inline animate_transform & animate_transform::type( const std::string & t )
{
	_type = t;
	return *this;
}

inline void animate_transform::write( std::ostream & os ) const
{
	std::map<std::string, std::string>::const_iterator it;

	os << "<animateMotion";
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	if ( !href().empty() )
	{
		os << " xlink:href=\"" << href() << "\"";
	}
	if ( !attribute_name().empty() )
	{
		os << " attributeName=\"" << attribute_name() << "\"";
	}
	if ( !attribute_type().empty() )
	{
		os << " attributeType=\"" << attribute_type() << "\"";
	}
	if ( !begin().empty() )
	{
		os << " begin=\"" << begin() << "s\"";
	}
	if ( !dur().empty() )
	{
		os << " dur=\"";
		if ( !std::isinf( double( dur() ) ) )
			os << dur() << "s";
		else
			os << "indefinite";
		os << "\"";
	}
	if ( !end().empty() )
	{
		os << " end=\"" << end() << "s\"";
	}
	if ( !min().empty() )
	{
		os << " min=\"" << min() << "s\"";
	}
	if ( !max().empty() )
	{
		os << " max=\"" << max() << "s\"";
	}
	if ( !restart().empty() )
	{
		os << " restart=\"" << restart() << "\"";
	}
	if ( !repeat_count().empty() )
	{
		os << " repeatCount=\"" << repeat_count() << "\"";
	}
	if ( !repeat_dur().empty() )
	{
		os << " repeatDur=\"" << repeat_dur() << "s\"";
	}
	if ( !fill().empty() )
	{
		os << " fill=\"" << fill() << "\"";
	}
	if ( !from().empty() )
	{
		os << " from=\"" << from() << "\"";
	}
	if ( !to().empty() )
	{
		os << " to=\"" << to() << "\"";
	}
	if ( !type().empty() )
	{
		os << " type=\"" << type() << "\"";
	}
	for ( it = attrs().begin(); it != attrs().end(); ++it )
	{
		os << " " << it->first << "=\"" << it->second << "\"";
	}
	os << "/>";
}

////////////////////////////////////////////////////////////////////////////////

inline document::document( double w, double h ) :
	element<document>(),
	_width( w ), _height( h ),
	_vbx( 0 ), _vby( 0 ),
	_vbw( w ), _vbh( h )
{
}

inline document::document( const document & e ) :
	element<document>( e ),
	_width( e._width ),_height( e._height ),
	_vbx( e._vbx ), _vby( e._vby ),
	_vbw( e._vbw ), _vbh( e._vbh )
{
}

inline document::~document()
{
	for ( int i = 0; i < int( _defs.size() ); ++i )
	{
		delete _defs[i];
	}
}

inline std::string document::name() const
{
	return "svg";
}

inline double document::width() const
{
	return _width;
}

inline double document::height() const
{
	return _height;
}

inline std::string document::view_box() const
{
	std::ostringstream oss;
	oss << _vbx << ' ' << _vby << ' ' << _vbw << ' ' << _vbh;
	return oss.str();
}

inline document & document::width( double w )
{
	_width = w;
	return *this;
}

inline document & document::height( double h )
{
	_height = h;
	return *this;
}

inline document & document::view_box( double x, double y, double w, double h )
{
	_vbx = x;
	_vby = y;
	_vbw = w;
	_vbh = h;
	return *this;
}

template<class D>
inline void document::append( const element<D> & e )
{
	_oss << '\t';
	e.write( _oss );
	_oss << std::endl;
}

template<class D>
inline void document::define( const element<D> & e )
{
	typedef typename element<D>::derived_type element_type;
	element_type * ptr = new element_type( static_cast<const element_type&>( e ) );
	_defs.push_back( ptr );
}

inline document & document::css( const std::string & selector, const std::string & key, const std::string & value )
{
	_css[ selector ][ key ] = value;
	return *this;
}

inline void document::write( std::ostream & os ) const
{
	std::map<std::string, std::map<std::string, std::string> >::const_iterator i;
	std::map<std::string, std::string>::const_iterator j;

	// Header
	os
		<< "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << std::endl
		<< "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\""
		<< " width=\"" << width() << "\" height=\"" << height() << "\""
		<< " viewBox=\"" << view_box() << "\"";
	if ( !id().empty() )
	{
		os << " id=\"" << id() << "\"";
	}
	for ( j = attrs().begin(); j != attrs().end(); ++j )
	{
		os << " " << j->first << "=\"" << j->second << "\"";
	}
	os << ">" << std::endl;

	if ( !_css.empty() || !_defs.empty() )
	{
		// Definitions
		os
			<< "\t<defs>" << std::endl
			<< "\t\t<style type=\"text/css\"><![CDATA[" << std::endl;
		for ( i = _css.begin(); i != _css.end(); ++i )
		{
			os << "\t\t\t" << i->first << std::endl;
			os << "\t\t\t{" << std::endl;
			for ( j = i->second.begin(); j != i->second.end(); ++j )
			{
				os << "\t\t\t\t" << j->first << ": " << j->second << ";" << std::endl;
			}
			os << "\t\t\t}" << std::endl;
		}
		os
			<< "\t\t]]></style>" << std::endl;
		for ( int i = 0; i < int( _defs.size() ); ++i )
		{
			os << "\t\t";
			_defs[i]->write( os );
			os << std::endl;
		}
		os
			<< "\t</defs>" << std::endl;
	}

	// Body
	os << _oss.str();

	os << "</svg>" << std::endl;
}

template<class D>
inline document & operator << ( document & doc, const element<D> & e )
{
	doc.append( e );
	return doc;
}

}

#endif
